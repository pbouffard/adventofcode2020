module Day10
using AdventOfCode2020
using StatsBase
using Test
using LightGraphs, SimpleWeightedGraphs

get_input() = get_input(read(datafile(10), String))
get_input(s::String) = sort(parse.(Int, split(s, "\n", keepempty=false)))

testdata = [
["""
16
10
15
5
1
11
7
19
6
12
4
""", Dict(3 => 5, 1 => 7), 8],
["""
28
33
18
42
31
14
46
20
48
47
24
23
49
45
19
38
39
11
1
32
25
35
8
17
7
9
4
2
34
10
3
""", Dict(1 => 22, 3 => 10), 19208],
]

jolt_differences(adapters) = countmap(diff(adapters))

adapters(input) = [0; input; maximum(input) + 3]

function test_part1()
  for (txt, result, _) in testdata
    input = get_input(txt)
    as = adapters(input)
    @test jolt_differences(as) == result
  end
  @info "Part 1 tests passed ✅"
end

function test_part2()
  for (txt, _, result) in testdata
    input = get_input(txt)
    @test part2(input) == result
  end
  @info "Part 2 tests passed ✅"
end

part1(input) = prod(values(jolt_differences(input)))
part1() = part1(get_input())

function nextvalid(jolts, inputset)
  #collect(intersect(collect(jolts .+ (1:3)), inputset))
  [(i, a) for (i, a) in enumerate(inputset) if a - jolts ∈ 1:3]
end

function test_nextvalid()
  @testset begin
    @test nextvalid(0, [1, 2, 3]) == [(1,1), (2,2), (3,3)]
    @test nextvalid(0, [2, 3, 4]) == [(1,2), (2,3)]
    @test nextvalid(0, [2, 3, 4]) == [(1,2), (2,3)]
  end
end 

function part2(input)
  as = adapters(input)
  g = SimpleWeightedDiGraph(length(as))
  # @assert add_edge!(g, 1, 2, 0.5)
  add_vertices!(g, length(as))
  # as_set = BitSet(as)
  for i in 1:length(as)
    nv = nextvalid(as[i], as)
    for (j, na) in nv
      @assert add_edge!(g, i, j, na - as[i])
    end
  end
  # (g, as)
  countpaths(g, 1, length(as))
end

function countpaths(g, src, dest)
  # following is only *almost* right, AFAICT!
  # https://www.geeksforgeeks.org/count-possible-paths-two-vertices/
  cache = Dict{Int,Int}()
  function countpathsutil!(u, pathcount)
    # visited[u] = true
    u in keys(cache) && return cache[u]
    # @show u, dest, pathcount, cache
    if u == dest
      pathcount += 1
    else
      ns = outneighbors(g, u)
      # @show ns
      for n in ns
        # @show n
        pathcount += countpathsutil!(n, pathcount)
      end
    end
    cache[u] = pathcount
    pathcount
  end

  
  # visited = falses(nv(g))
  pathcount = countpathsutil!(src, 0)
end

function run_tests()
  @testset begin
    test_part1()
    test_part2()
  end
end

part2() = part2(get_input())
part2test() = part2(get_input(testdata[1][1]))

ANSWERS = (2470, 2644613988352)
end