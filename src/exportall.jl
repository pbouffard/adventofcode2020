# https://discourse.julialang.org/t/exportall/4970/16?u=airpmb
for n in names(@__MODULE__; all=true)
  # @show n
  if Base.isidentifier(n) && n ∉ (Symbol(@__MODULE__), :eval, :include)
    # @info "Exporting $n"
    @eval export $n
  end
end