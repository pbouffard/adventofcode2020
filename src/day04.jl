module Day04
using AdventOfCode2020

get_input() = get_input(read(datafile(4), String))
get_input(data) = Dict.(map.(x->Pair(split(x, ":")...), split.(split(data, "\n\n"))))

fields = Dict(map(x -> Pair(split(x, limit=2)...), filter(!isempty, split("""
byr (Birth Year)
iyr (Issue Year)
eyr (Expiration Year)
hgt (Height)
hcl (Hair Color)
ecl (Eye Color)
pid (Passport ID)
cid (Country ID)
""", "\n"))))

fields_without_cid = setdiff(keys(fields), ["cid"])

testinput() = get_input("""
ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
byr:1937 iyr:2017 cid:147 hgt:183cm

iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
hcl:#cfa07d byr:1929

hcl:#ae17e1 iyr:2013
eyr:2024
ecl:brn pid:760753108 byr:1931
hgt:179cm

hcl:#cfa07d eyr:2025 pid:166559648
iyr:2011 ecl:brn hgt:59in
""")

function fields_present(passport)
  pkeys = keys(passport)
  all([field in pkeys for field in fields_without_cid])
end

function part1(input)
  count(map(fields_present, input))
end
part1() = part1(get_input())

function part1test() 
  p1test = part1(testinput())
  @show p1test
  @assert p1test == 2
  @info "Part 1 passed using AoC sample input"
end

year_between(year_str, atleast, atmost) = length(year_str) == 4 && atleast <= parse(Int, year_str) <= atmost

function fieldvalid(field, value)
  if field == "byr"
    return year_between(value, 1920, 2002)
  elseif field == "iyr"
    return year_between(value, 2010, 2020)
  elseif field == "eyr"
    return year_between(value, 2020, 2030)
  elseif field == "hgt"
    unit = value[end-1:end]
    qty = parse(Int, value[1:end-2])
    if unit == "cm"
      return 150 <= qty <= 193
    elseif unit == "in"
      return 59 <= qty <= 176
    else
      # @warn "Unexpected unit $unit"
      return false
    end
  elseif field == "hcl"
    value[1] == '#' && length(value) == 7 || return false
    return all([x in 'a':'f' || isdigit(x) for x in value[2:end]])
  elseif field == "ecl"
    return value in split("amb blu brn gry grn hzl oth")
  elseif field == "pid"
    return length(value) == 9 && all(isnumeric.(collect(value)))
  elseif field == "cid"
    return true
  else
    @warn "Unexpected field $field:$value"
  end
end

function passportvalid(passport)
  return fields_present(passport) && all([fieldvalid(f, v) for (f, v) in passport])
end

invalid_passports = """
eyr:1972 cid:100
hcl:#18171d ecl:amb hgt:170 pid:186cm iyr:2018 byr:1926

iyr:2019
hcl:#602927 eyr:1967 hgt:170cm
ecl:grn pid:012533040 byr:1946

hcl:dab227 iyr:2012
ecl:brn hgt:182cm pid:021572410 eyr:2020 byr:1992 cid:277

hgt:59cm ecl:zzz
eyr:2038 hcl:74454a iyr:2023
pid:3556412378 byr:2007
"""

function test_invalid_passports()
  passports = get_input(invalid_passports)
  valid = passportvalid.(passports)
  @assert count(valid) == 0
end

valid_passports = """
pid:087499704 hgt:74in ecl:grn iyr:2012 eyr:2030 byr:1980
hcl:#623a2f

eyr:2029 ecl:blu cid:129 byr:1989
iyr:2014 pid:896056539 hcl:#a97842 hgt:165cm

hcl:#888785
hgt:164cm byr:2001 iyr:2015 cid:88
pid:545766238 ecl:hzl
eyr:2022

iyr:2010 hgt:158cm hcl:#b6652a ecl:blu byr:1944 eyr:2021 pid:093154719
"""

function test_valid_passports()
  passports = get_input(valid_passports)
  valid = passportvalid.(passports)
  @assert count(valid) == length(passports)
end


function part2(passports)
  count(passportvalid.(passports))
end
part2() = part2(get_input())


function run_tests()
  part1(testinput()) == 2
  test_invalid_passports()
  test_valid_passports()
end


ANSWERS = (196, 114)

end