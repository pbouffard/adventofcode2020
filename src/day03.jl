module Day03
using AdventOfCode2020

get_input() = get_input(eachline(datafile(3)))
get_input(data) = hcat(map(x -> collect(x) .== '#', data)...)'

testinput = get_input(split("""
..##.......
#...#...#..
.#....#..#.
..#.#...#.#
.#...##..#.
..#.##.....
.#.#.#....#
.#........#
#.##...#...
#...##....#
.#..#...#.#
"""))

function checkslope(input, right, down)
  (rows, cols) = size(input)
  nt = 0 # number of trees
  i = 1 # "row"
  j = 1 # "column"
  while i < rows
    j = mod1(j+right, cols)
    i += down
    i <= rows && input[i, j] && (nt += 1)
  end
  nt
end

function part1(input)
  checkslope(input, 3, 1)
end
part1() = part1(get_input())

function part1test() 
  @assert part1(testinput) == 7
  @info "Part 1 passed using AoC sample input"
end



function part2(input)
  slopes = (
    (1, 1), 
    (3, 1), 
    (5, 1), 
    (7, 1),
    (1, 2)
    )
  answers = map(((right, down),) -> checkslope(input, right, down), slopes)
  # @show answers
  prod(answers)
end
part2() = part2(get_input())

const test_outputs = [7, 336]

run_tests() = part1test()

ANSWERS = (169, 7560370818)

end