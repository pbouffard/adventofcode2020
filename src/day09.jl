module Day09

using AdventOfCode2020
using IterTools

get_input() = get_input(eachline(datafile(9)))
get_input(data) = collect(parse(Int, x) for x in data)
get_input(s::String) = get_input(split(s, "\n", keepempty=false))

testdata = (
  N_preamble=5,
  data="""
    35
    20
    15
    25
    47
    40
    62
    55
    65
    95
    102
    117
    150
    182
    127
    219
    299
    277
    309
    576
    """,
    expected_result_part1 = 127,
    expected_result_part2 = 62,
)

test_part1() = part1(get_input(testdata.data), preamble_length=testdata.N_preamble) == testdata.expected_result_part1

function part1(input; preamble_length=25)
  N = length(input)
  cinput = collect(input)
  # @show cinput
  i = preamble_length+1
  while i <= N
    # @show i, cinput[i]
    nums = @view cinput[i - preamble_length:i-1]
    products = Iterators.product(nums, nums)
    # @show collect(products)
    if any(cinput[i] == sum(p) for p in products if p[1] != p[2])
      i += 1
    else
      break
    end
  end
  cinput[i]
end
part1() = part1(get_input())


function run_tests()
  test_part1()
  test_part2()
end

function part2(input; preamble_length=25)
  target = part1(input; preamble_length)
  head = 2
  tail = 1
  # @show view(input, tail:head)
  while (σ = sum(view(input, tail:head))) != target
    σ < target ? head += 1 : tail += 1
  end
  sum(extrema(view(input, tail:head)))
end
part2() = part2(get_input())

test_part2() = part2(get_input(testdata.data), preamble_length=testdata.N_preamble) == testdata.expected_result_part2

ANSWERS = (27911108, 4023754)
  
end