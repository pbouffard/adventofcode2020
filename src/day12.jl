module Day12
using AdventOfCode2020
using Base.Iterators: peel
using StaticArrays
using Test

get_input(txt) = split(txt, "\n", keepempty=false)
get_input() = get_input(read_datafile(12))

testdata = """
F10
N3
F7
R90
F11
"""

@enum CardinalDirection east north west south
@enum Turn left right
directions = Dict(east => SA[1, 0], north => SA[0, 1], west => SA[-1, 0], south => SA[0, -1])
direction_chars = Dict('E' => east, 'N' => north, 'W' => west, 'S' => south)
# turns = Dict(left => -90, right => 90)
turn_chars = Dict('L' => -1, 'R' => 1)
angle_to_dir = Dict(90 => east, 0 => north, -90 => west, 180 => south)

struct State
  east_north
  dir
  State() = new(SA[0, 0], 90)
  State(args...) = new(args...)
end

mdist(a::State, b::State) = sum(abs.(a.east_north - b.east_north))
wrap180(x) = mod(x, -179:180)

function part1(input)
  state = State()
  for move in input
    (instruction, amount_txt) = peel(move)
    amount = parse(Int, String(collect(amount_txt)))
    @show move, instruction, amount
    if instruction in keys(direction_chars)
      state = State(state.east_north + amount*directions[direction_chars[instruction]], state.dir)
    elseif instruction in keys(turn_chars)
      new_dir = wrap180(state.dir + turn_chars[instruction]*amount)
      state = State(state.east_north, new_dir)
    elseif instruction == 'F'
      state = State(state.east_north + amount*directions[angle_to_dir[state.dir]], state.dir)
    else
      error("Invalid instruction $instruction")
    end
    @show state, mdist(state, State())
  end
  mdist(state, State())
end
part1() = part1(get_input())

function run_tests()
  @test part1(get_input(testdata)) == 25
end

function part2(input)
  
end
part2() = part2(get_input())

ANSWERS = (1177, nothing)

# Uncomment to let names from this module show up in REPL (after `using .DayXX`) during development:
# include("exportall.jl")

end