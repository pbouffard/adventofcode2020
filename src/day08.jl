module Day08
using AdventOfCode2020

get_input() = eachline(datafile(8))
get_input(data) = split(data, "\n", keepempty=false)

testdata = """
nop +0
acc +1
jmp +4
acc +3
jmp -3
acc -99
acc +1
jmp -4
acc +6
"""

test_part1() = part1(get_input(testdata)) == 5

testdata2 = """
nop +0
"""

test_noloop() = loops_infinitely(parse_program(get_input(testdata2)))

mutable struct ProgramState
  p::Int # program counter
  acc::Int # accumulator
  ProgramState() = new(1, 0)
end

@enum Opcode NOP ACC JMP

struct Instruction
  op::Opcode
  arg::Int
end

function operate!(pstate, instruction)
  @debug pstate, instruction
  if instruction.op == NOP
    pstate.p += 1
  elseif instruction.op == ACC
    pstate.acc += instruction.arg
    pstate.p += 1
  elseif instruction.op == JMP
    pstate.p += instruction.arg
  end
  @debug pstate
end

function loops_infinitely(program)
  pstate = ProgramState()
  N = length(program)
  visited = BitSet()
  while pstate.p ∉ visited && pstate.p <= N
    push!(visited, pstate.p)
    @debug program[pstate.p], pstate.acc, visited
    operate!(pstate, program[pstate.p])
  end
  (pstate.p <= N, pstate.acc)
end

parse_program(data) = map(data) do line
  ls = split(line)
  Instruction(getproperty(@__MODULE__, Symbol(uppercase(ls[1]))), parse(Int, ls[2]))
end

function part1(data)
  program = parse_program(data)
  (result, acc) = loops_infinitely(program)
  @assert result
  acc
end

part1() = part1(get_input())

function part2(data)
  program = parse_program(data)
  N = length(program)
  for i ∈ 1:N
    original_instruction = program[i]
    if original_instruction.op ∈ (NOP, JMP)
      program[i] = Instruction(original_instruction.op == JMP ? NOP : JMP, original_instruction.arg)
      (infinite, acc) = loops_infinitely(program)
      !infinite && return acc
      program[i] = original_instruction
    end
  end
end
part2() = part2(get_input())

test_part2() = part2(get_input(testdata)) == 8

function run_tests()
  test_part1()
  test_noloop()
  test_part2()
end

begin
  
end

ANSWERS = (1584, 920)

end