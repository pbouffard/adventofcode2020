# https://adventofcode.com/2020/day/11
module Day11
using AdventOfCode2020
using IterTools
using Test


Broadcast.broadcastable(I::CartesianIndex) = Ref(I) # see https://discourse.julialang.org/t/tuples-of-cartesianindex-and-broadcasting/50093

const CI = CartesianIndex

const FLOOR = 0
const EMPTY = 1
const OCCUPIED = 2

const FLOORCHAR = '.'
const EMPTYCHAR = 'L'
const OCCUPIEDCHAR = '#'

const CHARMAP = Dict(FLOORCHAR => FLOOR, EMPTYCHAR => EMPTY, OCCUPIEDCHAR => OCCUPIED)

get_input() = get_input(read(datafile(11), String))
get_input(s::AbstractString) = hcat(EMPTY * map(x -> [CHARMAP[c] for c in collect(x)], split(s, "\n", keepempty=false))...)'
test_input() = get_input(testdata)

# ! PART 1

testdata = split("""
L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL

#.##.##.##
#######.##
#.#.#..#..
####.##.##
#.##.##.##
#.#####.##
..#.#.....
##########
#.######.#
#.#####.##


#.LL.L#.##
#LLLLLL.L#
L.L.L..L..
#LLL.LL.L#
#.LL.LL.LL
#.LLLL#.##
..L.L.....
#LLLLLLLL#
#.LLLLLL.L
#.#LLLL.##


#.##.L#.##
#L###LL.L#
L.#.#..#..
#L##.##.L#
#.##.LL.LL
#.###L#.##
..#.#.....
#L######L#
#.LL###L.L
#.#L###.##

#.#L.L#.##
#LLL#LL.L#
L.L.L..#..
#LLL.##.L#
#.LL.LL.LL
#.LL#L#.##
..L.L.....
#L#LLLL#L#
#.LLLLLL.L
#.#L#L#.##

#.#L.L#.##
#LLL#LL.L#
L.#.L..#..
#L##.##.L#
#.#L.LL.LL
#.#L#L#.##
..L.L.....
#L#L##L#L#
#.LLLLLL.L
#.#L#L#.##
""", "\n\n", keepempty=false)

neighbor_directions = setdiff(collect(product((-1,0,1),(-1,0,1)))[:], [(0, 0),])
neighbor_indices = CartesianIndex.(neighbor_directions)
adjacents(loc) = map(x -> loc .+ x, neighbor_indices)
adjacents(loc, M) = filter(x -> checkbounds(Bool, M, x), adjacents(loc))

# within_bounds(loc, upper_left, lower_right) = all(upper_left.I .<= loc.I .<= lower_right.I)

"""
If a seat is empty (L = 1) and there are no occupied seats adjacent to it, the seat becomes occupied.
"""
rule1(M, ci; debug=false) = (M[ci] == EMPTY) && !any(M[adjacents(ci, M)] .== OCCUPIED)
"""
If a seat is occupied (# = 2) and four or more seats adjacent to it are also occupied, the seat becomes empty.
"""
function rule2(M, ci; debug=false)
  if M[ci] == OCCUPIED
    adj_occupied = M[adjacents(ci, M)] .== OCCUPIED
    # @show adj_occupied, sum(adj_occupied)
    if sum(adj_occupied) ≥ 4
      return true
    end
  end
  return false
end

"""
Perform one iteration (part 2)
"""
function iterate_layout(M, rule_a, result_a, rule_b, result_b)
  M_out = copy(M)
  for i ∈ 1:size(M, 1), j ∈ 1:size(M, 2)
    # @show typeof(i)
    ci = CI(i,j)
    rule_a_result = rule_a(M, ci)
    rule_b_result = rule_b(M, ci)
    # @show (i, j), rule1result, rule2result
    if rule_a_result
      M_out[ci] = result_a
    elseif rule_b_result
      M_out[ci] = result_b
    end
  end
  M_out
end

"""
Perform `n` iterations (Part 2)
"""
function iterate_layout(M, n, rule_a, result_a, rule_b, result_b)
  M_out = copy(M)
  for i ∈ 1:n
    M_out = iterate_layout(M_out, rule_a, result_a, rule_b, result_b)
    # @show M_out
  end
  M_out
end

function part1(M)
  M_prev = copy(M)
  i = 1
  while true
    M_next = iterate_layout(M_prev, rule1, OCCUPIED, rule2, EMPTY)
    occupied = count(==(OCCUPIED), M_prev)
    # @show i, occupied
    M_next == M_prev && return occupied
    M_prev = copy(M_next)
    i += 1
  end
end
test_part1() = part1(get_input(testdata[1])) == 37
part1() = part1(get_input())

# ! PART 2

testdata2 = split("""
L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL

#.##.##.##
#######.##
#.#.#..#..
####.##.##
#.##.##.##
#.#####.##
..#.#.....
##########
#.######.#
#.#####.##

#.LL.LL.L#
#LLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLL#
#.LLLLLL.L
#.LLLLL.L#

#.L#.##.L#
#L#####.LL
L.#.#..#..
##L#.##.##
#.##.#L.##
#.#####.#L
..#.#.....
LLL####LL#
#.L#####.L
#.L####.L#

#.L#.L#.L#
#LLLLLL.LL
L.L.L..#..
##LL.LL.L#
L.LL.LL.L#
#.LLLLL.LL
..L.L.....
LLLLLLLLL#
#.LLLLL#.L
#.L#LL#.L#

#.L#.L#.L#
#LLLLLL.LL
L.L.L..#..
##L#.#L.L#
L.L#.#L.L#
#.L####.LL
..#.#.....
LLL###LLL#
#.LLLLL#.L
#.L#LL#.L#

#.L#.L#.L#
#LLLLLL.LL
L.L.L..#..
##L#.#L.L#
L.L#.LL.L#
#.LLLL#.LL
..#.L.....
LLL###LLL#
#.LLLLL#.L
#.L#LL#.L#
""", "\n\n", keepempty=false)

"""
(Rule 3) As soon as people start to arrive, you realize your mistake. People don't just
care about adjacent seats - they care about the first seat they can see in each
of those eight directions!

Returns true if the first seat visible in all of the 8 directions is unoccupied.

(in Part 2, replaces Rule 1)
"""
function rule3(M, ci; debug=false)
  if M[ci] != EMPTY
    if debug
      @warn "Seat occupied, rule 3 inapplicable."
    end
    return false # inapplicable
  end
  function occupied_in_direction(dir)
    (found, distance, loc) = first_visible_seat(dir, M, ci)
    if debug; @info "Called occupied_in_direction($dir) from index $ci; found=$found, distance=$distance, loc=$loc, M[loc]=$(found ? M[loc] : "N/A")"; end
    found && M[loc] == OCCUPIED
  end
  !any(occupied_in_direction(dir) for dir in neighbor_indices)
end

function test_rule3()
  @testset begin
    @test rule3([0 1 0; 0 1 0; 0 1 0], CI(2,2)) == true
    @test rule3([1 1 0; 0 1 0; 0 1 1], CI(2,2)) == true
    @test rule3([2 1 0; 0 1 0; 0 1 1], CI(2,2)) == false
    @test rule3([1 1 1; 1 1 1; 1 1 1], CI(2,2)) == true
  end
end

""" For the given layout `M`, and starting location `ci`, find the first visible
seat (non-floor location), in the given direction `dir`. Return named tuple of
true if a seat was found, the number of steps taken to the seat if so, and the
resulting index `ci′` into the layout. If a seat is not found (found field of
tuple is false) then the distance and `ci′` will represent an invalid index one
step out of the layout.
"""
function first_visible_seat(dir, M, ci)
  # @info "first_visible_seat($dir, $M, $ci)"
  ci′ = ci
  distance = 0
  found = false
  while true
    ci′ += dir
    distance += 1
    inbounds = checkbounds(Bool, M, ci′)
    Mci_if_inbounds = inbounds ? M[ci′] : "out of bounds"
    # @show ci′, inbounds, Mci_if_inbounds
    if !inbounds
      break
    elseif M[ci′] ≠ FLOOR
      found = true
      break
    end
  end
  (;found, distance, ci′)
end 

function test_first_visible_seat()
  empty_layout = [0 0 0; 0 0 0; 0 0 0]
  # @testset begin
    @test first_visible_seat(CI(1, 0), empty_layout, CI(1,1)) === (found=false, distance=3, ci′=CI(4,1))
    @test first_visible_seat(CI(0, 1), empty_layout, CI(1,1)) === (found=false, distance=3, ci′=CI(1,4))
    @test first_visible_seat(CI(1, 1), empty_layout, CI(2,2)) === (found=false, distance=2, ci′=CI(4,4))
    @test first_visible_seat(CI(0, 1), [0 0 0; 0 0 1; 0 0 0], CI(2,2)) === (found=true, distance=1, ci′=CI(2,3))
    @test first_visible_seat(CI(1, 1), [0 0 0; 0 0 1; 0 0 0], CI(2,2)) === (found=false, distance=2, ci′=CI(4,4))
    @test first_visible_seat(CI(-1, 0), [0 0 1; 0 0 1; 0 0 0], CI(3, 3)) === (found=true, distance=1, ci′=CI(2,3))
    @test first_visible_seat(CI(-1, 0), [0 0 1; 0 0 2; 0 0 0], CI(3, 3)) === (found=true, distance=1, ci′=CI(2,3))
    @test first_visible_seat(CI(-1, 0), [0 0 1; 0 0 2; 0 0 0], CI(3, 3)) === (found=true, distance=1, ci′=CI(2,3))
    @test first_visible_seat(CI(-1, -1), [2 0 0; 0 0 0; 0 0 1], CI(2,2)) === (found=true, distance=1, ci′=CI(1,1))
    @test first_visible_seat(CI(-1, -1), [2 2 2; 0 1 0; 2 0 0], CI(2,2)) === (found=true, distance=1, ci′=CI(1,1))

    @test first_visible_seat(CI(0, -1), [2 2 2; 0 1 0; 2 0 0], CI(2,2)) === (found=false, distance=2, ci′=CI(2,0))
    @test first_visible_seat(CI(1, -1), [2 2 2; 0 1 0; 2 0 0], CI(2,2)) === (found=true, distance=1, ci′=CI(3,1))
    @test first_visible_seat(CI(1, 0), [2 2 2; 0 1 0; 2 0 0], CI(2,2)) === (found=false, distance=2, ci′=CI(4,2))
  # end
end

"""
(Rule 4) Also, people seem to be more tolerant than you expected: it now takes five or
more visible occupied seats for an occupied seat to become empty (rather than
four or more from the previous rules). The other rules still apply: empty seats
that see no occupied seats become occupied, seats matching no rule don't change,
and floor never changes.

Return `true` if 5 or more visible seats are occupied.
"""
function rule4(M, ci; debug=false)
  if debug
    @info("rule4(M, $ci), M = ")
    @info display(M)
  end
  if M[ci] != OCCUPIED
    if debug
      @warn "Seat not occupied, rule4 inapplicable"
    end
    return false
  end
  n_occupied = 0
  for dir in neighbor_indices
    (found, distance, loc) = first_visible_seat(dir, M, ci)
    loc_found = found ? M[loc] : "N/A"
    if debug
      @show dir, found, distance, loc, loc_found
    end
    if found && M[loc] == OCCUPIED
      # @warn "Occupied visible seat at $loc"
      n_occupied += 1
      n_occupied >= 5 && return true
    end
  end
  return false
end

function test_rule4()
  @testset begin
    @test rule4([2 2 2; 0 1 0; 2 0 0], CI(2,2)) == false
    @test rule4([2 2 2; 0 2 0; 2 0 0], CI(2,2)) == false
    @test rule4([2 2 2; 0 2 0; 2 0 1], CI(2,2)) == false
    @test rule4([2 2 2; 0 2 0; 2 0 2], CI(2,2)) == true
    @test rule4([1 0 0; 0 2 0; 0 0 1], CI(2,2)) == false
    @test rule4([2 0 0; 0 2 0; 0 0 1], CI(2,2)) == false
    @test rule4([1 1 1; 1 2 1; 1 1 1], CI(2,2)) == false
  end
end


function test_iterate_layout(rule_a, rule_b, testdata)
  layouts = get_input.(testdata)
  M = layouts[1]
  @testset begin
    for i in 1:length(layouts)-1
      M, M_expected = layouts[i:i+1]
      M_iterated = iterate_layout(M, rule_a, OCCUPIED, rule_b, EMPTY)

      @warn "Iteration $i\nBEFORE:"
      display(M)
      @info "AFTER, actual:"
      display(M_iterated)
      @info "Diff:"
      Mdiff = M_iterated - M_expected
      display(Mdiff)
      @info "AFTER, Expected:"
      display(M_expected)
      if M_iterated != M_expected
        first_wrong = findfirst(Mdiff .!= 0)
        @warn "First wrong index $first_wrong, $(M[first_wrong]) -> $(M_iterated[first_wrong]) but expected $(M_expected[first_wrong])"
        @show rule_a(M, first_wrong; debug=true), rule_b(M, first_wrong; debug=true)
        @error "first_wrong = $first_wrong"
      end
      @test M_iterated == M_expected
    end
    for i in length(layouts):length(layouts)+3
      @test iterate_layout(M, i, rule_a, OCCUPIED, rule_b, EMPTY) == iterate_layout(M, i+1, rule_a, OCCUPIED, rule_b, EMPTY)
    end
  end
end

function part2(M)
  M_prev = copy(M)
  i = 1
  while true
    M_next = iterate_layout(M_prev, rule3, OCCUPIED, rule4, EMPTY)
    occupied = count(==(OCCUPIED), M_prev)
    # @show i, occupied
    M_next == M_prev && return occupied
    M_prev = copy(M_next)
    i += 1
    i % 10 == 0 #&& @info sum(M_next .== M_prev)
  end
end
test_part2() = part2(get_input(testdata2[1])) == 26
part2() = part2(get_input())

function run_tests()
  @testset begin
    test_iterate_layout(rule1, rule2, testdata)
    test_part1()
    test_first_visible_seat()
    test_rule3()
    test_rule4()
    test_iterate_layout(rule3, rule4, testdata2)
    test_part2()
  end
end

ANSWERS = (2453, 2159)

# Uncomment to let names from this module show up in REPL (after `using .DayXX`) during development:
# include("exportall.jl")

end