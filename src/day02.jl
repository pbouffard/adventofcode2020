module Day02
using AdventOfCode2020

get_input() = readlines(datafile(2))

function part1(input)
  pattern = r"(\d+)-(\d+) ([a-z]): ([a-z]*)"
  numvalid = 0
  for line in input
      matches = match(pattern, line)
      n1, n2, letter, password = matches.captures
      L = first(letter)
      N = count(collect(password) .== L)
      (parse(Int, n1) <= N <= parse(Int,n2)) && (numvalid += 1)
  end
  numvalid
end
part1() = part1(get_input())

function part2(input)
  pattern = r"(\d+)-(\d+) ([a-z]): ([a-z]*)"
  numvalid = 0
  for line in input
      matches = match(pattern, line)
      # @show line
      # @show matches
      n1, n2 = parse.(Int, matches.captures[1:2])
      letter = first(matches.captures[3])
      password = matches.captures[4]
      ((password[n1] == letter) ⊻ (password[n2] == letter)) && (numvalid += 1)
  end
  numvalid
end
part2() = part2(get_input())

run_tests() = nothing

ANSWERS = (660, 530)

end