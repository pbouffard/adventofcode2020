module Day06

using AdventOfCode2020
using Base.Iterators: peel

get_input() = split(read(datafile(6), String), "\n\n")
get_input(data) = split(data, "\n\n")

letterspresent(s) = length(Set(collect(replace(s, "\n" => ""))))
part1(data) = sum(letterspresent.(data))
part1() = part1(get_input())

function everyone_yes(s)
  (ss1, ssrest) = peel(split(s, "\n"))
  sum(length.(intersect(ss1, ssrest...)))
end

part2(data) = sum(everyone_yes.(data))
part2() = part2(get_input())

testdata = """
abc

a
b
c

ab
ac

a
a
a
a

b
"""
testdata_letterspresent = [3, 3, 3, 1, 1]

function test_letterspresent()
  for (s, n) in zip(split(testdata, "\n\n"), testdata_letterspresent)
    @assert letterspresent(s) == n
  end
  @info "test_letterspresent tests passed ✅"
end
test_part1() = part1(testdata) == 11

function run_tests()
  test_letterspresent() 
  test_part1()
end

ANSWERS = (6457, 3260)

end