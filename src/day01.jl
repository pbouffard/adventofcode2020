module Day01
using AdventOfCode2020

get_input() = parse.(Int, split(read(datafile(1), String)))

function part1(input)
    for i in 1:length(input), j in i+1:length(input)
        if input[i] + input[j] == 2020
            return input[i] * input[j]
        end
    end
end

part1() = part1(get_input())

function part2(input)
    vset = Set(input)
    for i in 1:length(input), j in i+1:length(input)
        if (rest = 2020 - (input[i] + input[j])) > 0
          if rest in vset
            return prod((input[i], input[j], rest))
          end
        end
    end
end

run_tests() = nothing

part2() = part2(get_input())
ANSWERS = (776064, 6964490)

end