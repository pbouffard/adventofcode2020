module Day07

using AdventOfCode2020
using Base.Iterators: peel, partition
using DataStructures

get_input() = eachline(datafile(7))
get_input(data) = filter(!isempty, split(data, "\n"))

const ParseLineOutputDict = Dict{String, Int} # e.g. "dark blue" => ("pale red" => 2)
function parse_line(s)::Pair{String, ParseLineOutputDict}
  container, contents = split(s, " bags contain ")
  contents == "no other bags." && return container => ParseLineOutputDict()
  contents_pattern = r"(\d+) (\S+ \S+) bags?"
  output = container => Dict(String(m.captures[2]) => parse(Int, m.captures[1]) for m in eachmatch(contents_pattern, contents))
  # @show output
  return output
end

parse_line_testdata = [
  [
    "plaid beige bags contain 2 posh purple bags, 4 pale olive bags, 3 striped green bags, 5 bright orange bags.",
    "plaid beige" => Dict("posh purple" => 2, "pale olive" => 4, "striped green" => 3, "bright orange" => 5),
  ],
  [
    "muted coral bags contain 4 dark gold bags.",
    "muted coral" => Dict("dark gold" => 4),
  ],
  [
    "shiny teal bags contain no other bags.",
    "shiny teal" => Dict{String, Pair{String, Int}}(),
  ]
]

function test_parse_line()
  for (test_line, expected_result) in parse_line_testdata
    @assert parse_line(test_line) == expected_result
  end
end

function inner_to_outer(containers)
  result = DefaultDict{String, Set{String}}(Set{String})
  for (container, contents) in containers
    isempty(contents) && continue
    for (bag, qty) in contents
      push!(result[bag], container)
    end
  end
  result
end

function walk(D, start)
  q = [start]
  result = Set{String}()
  while !isempty(q)
    inside = pop!(q)
    for outside in D[inside]
      push!(q, outside)
      push!(result, outside)
    end
  end
  result
end

testdata = """
light red bags contain 1 bright white bag, 2 muted yellow bags.
dark orange bags contain 3 bright white bags, 4 muted yellow bags.
bright white bags contain 1 shiny gold bag.
muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
dark olive bags contain 3 faded blue bags, 4 dotted black bags.
vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
faded blue bags contain no other bags.
dotted black bags contain no other bags.
"""
expected = Set(["muted yellow", "dark orange", "light red", "bright white"])

outer_options(data, inner) = walk(inner_to_outer(parse_line.(data)), inner)

inner_to_outer() = inner_to_outer(parse_line.(get_input()))

part1_inner_bagcolor = "shiny gold"
test_outer_options() = @assert outer_options(get_input(testdata), part1_inner_bagcolor) == expected
test_part1() = @assert part1(get_input(testdata)) == length(expected)

part1(data) = length(outer_options(data, part1_inner_bagcolor))
part1() = part1(get_input())

testdata_part2 = """
shiny gold bags contain 2 dark red bags.
dark red bags contain 2 dark orange bags.
dark orange bags contain 2 dark yellow bags.
dark yellow bags contain 2 dark green bags.
dark green bags contain 2 dark blue bags.
dark blue bags contain 2 dark violet bags.
dark violet bags contain no other bags.
"""

function recurse(containers, start)
  q = [start => 1]
  n = -1 # don't include the initial bag
  while !isempty(q)
    (bag, qty) = pop!(q)
    n += qty
    for (inner_bag, inner_qty) in containers[bag]
      push!(q, (inner_bag => qty*inner_qty))
    end
  end
  return max(0, n) # handle corner case
end

test_part2() = part2(get_input(testdata_part2)) == 126

function part2(data)
  recurse(Dict(parse_line.(data)...), "shiny gold")
end

part2() = part2(get_input())

function run_tests()
  test_outer_options()
  test_part1()
  test_part2()
end

ANSWERS = (222, 13264)

end
