module Day05
using AdventOfCode2020

get_input() = eachline(datafile(5))
get_input(data) = split(data, "\n")


function charbinary(v, onechar)
  N = length(v)
  result = 0
  for (i, c) in enumerate(reverse(v))
    if c == onechar
      result += 2^(i-1)
    end
  end
  result
end

function test_charbinary()
  @assert charbinary("FBFBBFF", 'B') == 44
  @assert charbinary("RLR", 'R') == 5
  @info "charbinary tests passed ✅"
end

seatlocation(seatspec) = (charbinary(seatspec[1:7], 'B'), charbinary(seatspec[8:10], 'R'))

function test_seatlocation()
  tests = [
    ["FBFBBFFRLR", (44, 5), 357],
    ["BFFFBBFRRR", (70, 7), 567],
    ["FFFBBBFRRR", (14, 7), 119],
    ["BBFFBBFRLL", (102, 4), 820]
  ]
  for (seatspec, (row, col), seat_id) in tests
    @assert seatlocation(seatspec) == (row, col)
    @assert seatid(row, col) == seat_id
  end
  @info "seatlocation tests passed ✅"
end

seatid(row, col) = 8*row + col
seatid(rowcol) = seatid(rowcol[1], rowcol[2])
seatid(seatspec::String) = seatid(seatlocation(seatspec))

function test_functions()
  test_charbinary()
  test_seatlocation()
  @info "function tests passed ✅"
end

part1(input) = maximum(seatid.(input))
part1() = part1(get_input())

function part2(input)
  seatids = seatid.(input)
  for seatid in 1:((128*8)-1)
    (seatid ∉ seatids) && ((seatid-1) in seatids) && (seatid+1 in seatids) && return seatid
  end
end
part2() = part2(get_input())

run_tests() = test_functions()

ANSWERS = (901, 661)

end