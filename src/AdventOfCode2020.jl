module AdventOfCode2020
using Formatting
using BenchmarkTools
using InteractiveUtils

export datafile, bothparts, alldays, tests, benchmarks, bothparts, allbenchmarks, NUM_DAYS, answers, answers, warntype, allwarntype, @usingid, read_datafile

# @macro exportall(m::Module)
#     for n in names(m; all=true)
#         if Base.isidentifier(n) && n ∉ (Symbol(@__MODULE__), :eval, :include)
#             @eval export $n
#         end
#     end
# end

# @macro exportid(m::Module, id::String)
#     for n in names(m; all=true)
#         @eval export $n
#     end
# end

macro usingid(m, id)
    x = :(x = getproperty($m, Symbol($id)))
end

# macro mod()
#     return __module__
# end


NUM_DAYS = 12

for day in 1:NUM_DAYS
    dayXX = format("day{:02d}", day)
    DayXX = format("Day{:02d}", day)
    path = joinpath(@__DIR__, "$(dayXX).jl")
    if isfile(path)
        include(path)
        sdxx = Symbol(DayXX)
        @eval export $sdxx
    end
end

function datafile(day)
    joinpath(@__DIR__, format("../data/day{:02d}.txt", day)) |> abspath
end

read_datafile(day) = read(datafile(day), String)

function bothparts(part1::Function, part2::Function, part1expected, part2expected)
    p1ans = part1()
    @info "Part 1: $p1ans"
    @assert p1ans == part1expected
    p2ans = part2()
    @info "Part 2: $p2ans"
    @assert p2ans == part2expected
    @info "Both parts ✅"
end

day_n_module(n) = Symbol(format("Day{:02d}", n))

function alldays()
    for day in 1:NUM_DAYS
        @info "Day $(day)"
        ans = tests(day)
        @info ans
    end
    true
end

function day_n_functions(day)
    (
    (@eval $(day_n_module(day)).part1),
    (@eval $(day_n_module(day)).part2),
    )
end

function bothparts(day)
    tuple([part() for part in day_n_functions(day)]...)
end

function benchmarks(day)
    parts = day_n_functions(day)
    for (i, part) in enumerate(parts)
        @info "Timing part $(i)"
        @btime $(part)() seconds=1 samples=10
    end
    true
end

function answers(day)
    @eval $(day_n_module(day)).ANSWERS
end

function tests(day)
    # TODO: automatically run any function in the module that starts with test_
    @info "Day $(day) ..."
    parts = day_n_functions(day)
    ans = (@eval $(day_n_module(day)).ANSWERS)
    tests = (@eval $(day_n_module(day)).run_tests)
    tests()
    for (part, part_ans) in zip(parts, ans)
        @assert part() == part_ans
    end
    @info "... tests passed ✅; answers = $ans"
    ans
end

function warntype(day)
    parts = day_n_functions(day)
    for part in parts
        InteractiveUtils.@code_warntype part()
    end
end

function allwarntype()
    for day in 1:NUM_DAYS
        @info "Day $(day)"
        warntype(day)
    end
end

function allbenchmarks()
    for day in 1:NUM_DAYS
        @info "Day $(day)"
        benchmarks(day)
    end
    true
end

end # module