using AdventOfCode2020
using Test

@testset "run all" begin
  for day in 1:NUM_DAYS
    @test bothparts(day) == answers(day)
  end
end

@testset "benchmarks" begin
  for day in 1:NUM_DAYS
    @info "Day $(day) ..."
    @test benchmarks(day)
  end
end
